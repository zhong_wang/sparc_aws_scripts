# About #

These are a bunch of scripts that spin up aws EMR clusters and install SpaRC, Jupyter and common python libraries.

# Authors #

Zhong Wang

# Requirements #

  1. Create AWS account
  2. install Command Line Interface (CLI)
  3. Add credentials for programmatic access by running aws configure
  4. Get familiar with aws cli -- read documentation (S3, EMR)

# Usage #

## Launch a small cluster on AWS with SPARC and all software ##

Launch a cluster with 1 master and 2 core nodes, r3-xlarge using the provided script:

```
#!/bin/bash
# just SpaRC, XXXX is the name of the key (the part before .pem)
 ./aws-launch-sparc.bash --node r3.xlarge --size 2 --price 0.2 --release emr-5.12.0 --key XXXX

# SpaRC and Jupyter
 ./aws-launch-sparc.bash --node r3.xlarge --size 2 --price 0.2 --release emr-5.12.0 --key XXXX(required) --jupyter
```

You will see sth. Like this
{
    "ClusterId": "j-XXXXXXXXX"
}

It normally takes 15-20mins for the cluster to get ready. 
By default, the cluster is named "spark-sparc" or "spark-sparc-jupyter", from EMR, select the cluster to view its status.

## Run SpaRC ##

When cluster is ready, log into the master node, and start to run SpaRC. If you also have Jupter installed, start a jupyter-notebook there (using port forwarding). Sth like this:

```
#!/bin/bash
ssh -i XXX.pem -L 8888:localhost:8888 hadoop@ec2-54-161-108-93.compute-1.amazonaws.com
# Lauch jupyter notebook:
jupyter-notebook --no-browser
```

Copy the link to the browser on your local computer browser to start using the notebook.


