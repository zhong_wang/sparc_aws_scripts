#!/usr/bin/env bash
# spawn a small cluster to run spark applications
# by Zhong Wang @ lbl.gov

# some defaults
MASTER="r5.xlarge"
NODE="r5.xlarge"
VOL=32
SIZE=2
PRICE=0.2
KEY=""
JUPYTER=false

# usage message
usage ()
{
  echo "Launch an EMR cluster on AWS. Last updated 10/12/2018" 
  echo "Example: $0 --master r5.xlarge --node r5.xlarge --size 2 --volume 100 --price 0.2 --release emr-5.17.0 --key XXXX(required) --jupyter(optional)"
  exit 0
}

if [ $# -lt 4 ]; then
	usage
fi

# get input parameters
while [ $# -gt 0 ]; do
    case "$1" in
    --master)
      shift
      MASTER=$1
      ;;
    --node)
      shift
      NODE=$1
      ;;
    --size)
      shift
      SIZE=$1
      ;;
    --volume)
      shift
      VOL=$1
      ;;
    --price)
      shift
      PRICE=$1
      ;;
    --release)
      shift
      RELEASE=$1
      ;;
    --jupyter)
      JUPYTER=true
      ;;
    --key)
      shift
      KEY=$1
      ;;      
    -*)
      echo "Unknown options: $1"
      usage
      ;;
    *)
      break;
      ;;
    esac
    shift
done

if [ "$JUPYTER" = true ]; then 
echo "Launch a cluster with Jupyter Notebook"
aws emr create-cluster --release-label ${RELEASE} \
  --name "spark-sparc-jupyter" --no-visible-to-all-users \
  --applications Name=Hadoop Name=Spark Name=Ganglia Name=Zeppelin \
  --ec2-attributes KeyName=${KEY},SubnetId=subnet-01a65bbdd29523d1a,InstanceProfile=EMR_EC2_DefaultRole \
  --service-role EMR_DefaultRole \
  --instance-groups "InstanceGroupType=MASTER,InstanceCount=1,InstanceType=${MASTER},\
EbsConfiguration={EbsOptimized=true,EbsBlockDeviceConfigs=[{VolumeSpecification={VolumeType=gp2,SizeInGB=${VOL}}}]}" \
  "InstanceGroupType=CORE,InstanceCount=${SIZE},InstanceType=${NODE},BidPrice=${PRICE}" \
  --region us-east-1 \
  --log-uri s3://aws-logs-412986692990-us-east-1 \
  --bootstrap-actions Name="SpaRC-jupyter",Path="s3://sparkAssembler/artifacts/emr-boostrap-sparc-jupyter-cached.sh"
else
aws emr create-cluster --release-label ${RELEASE} \
  --name "spark-sparc-jupyter" --no-visible-to-all-users \
  --applications Name=Hadoop Name=Spark Name=Ganglia Name=Zeppelin \
  --ec2-attributes KeyName=${KEY},SubnetId=subnet-01a65bbdd29523d1a,InstanceProfile=EMR_EC2_DefaultRole \
  --service-role EMR_DefaultRole \
  --instance-groups "InstanceGroupType=MASTER,InstanceCount=1,InstanceType=${MASTER},\
EbsConfiguration={EbsOptimized=true,EbsBlockDeviceConfigs=[{VolumeSpecification={VolumeType=gp2,SizeInGB=${VOL}}}]}" \
  "InstanceGroupType=CORE,InstanceCount=${SIZE},InstanceType=${NODE},BidPrice=${PRICE}" \
  --region us-east-1 \
  --log-uri s3://aws-logs-412986692990-us-east-1 \
  --bootstrap-actions Name="SpaRC-jupyter",Path="s3://sparkAssembler/artifacts/emr-boostrap-sparc-cached.sh"
fi

