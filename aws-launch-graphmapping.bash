#!/usr/bin/env bash
# spawn a small cluster to run spark applications
# by Zhong Wang @ lbl.gov

# some defaults
NODE="r3.xlarge"
SIZE=2
PRICE=0.2
KEY=""
JUPYTER=false

# usage message
usage ()
{
  echo "Example: $0 --node r3.xlarge --size 2 --price 0.2 --release emr-5.1.0 --key XXXX(required) --jupyter(optional)"
  exit 0
}

if [ $# -lt 4 ]; then
	usage
fi

# get input parameters
while [ $# -gt 0 ]; do
    case "$1" in
    --node)
      shift
      NODE=$1
      ;;
    --size)
      shift
      SIZE=$1
      ;;
    --price)
      shift
      PRICE=$1
      ;;
    --release)
      shift
      RELEASE=$1
      ;;
    --jupyter)
      JUPYTER=true
      ;;
    --key)
      shift
      KEY=$1
      ;;      
    -*)
      echo "Unknown options: $1"
      usage
      ;;
    *)
      break;
      ;;
    esac
    shift
done

if [ "$JUPYTER" = true ]; then 
echo "Launch a cluster with Jupyter Notebook"
aws emr create-cluster --release-label ${RELEASE} \
  --name "spark-graph-mapping-jupyter" --no-visible-to-all-users \
  --applications Name=Hadoop Name=Spark Name=Ganglia Name=HBase Name=JupyterHub \
  --ec2-attributes KeyName=${KEY},InstanceProfile=EMR_EC2_DefaultRole \
  --service-role EMR_DefaultRole \
  --instance-groups InstanceGroupType=MASTER,InstanceCount=1,InstanceType=r3.xlarge \
  InstanceGroupType=CORE,InstanceCount=${SIZE},InstanceType=${NODE},BidPrice=${PRICE} \
  --region us-east-1 \
  --bootstrap-actions Name="graphmap",Path="s3://zhong-active/graphmapping/emr-boostrap-graphmapping.sh"
else
aws emr create-cluster --release-label ${RELEASE} \
  --name "spark-graph-mapping" --no-visible-to-all-users \
  --applications Name=Hadoop Name=Spark Name=Ganglia Name=HBase \
  --ec2-attributes KeyName=${KEY},InstanceProfile=EMR_EC2_DefaultRole \
  --service-role EMR_DefaultRole \
  --instance-groups InstanceGroupType=MASTER,InstanceCount=1,InstanceType=r3.xlarge \
  InstanceGroupType=CORE,InstanceCount=${SIZE},InstanceType=${NODE},BidPrice=${PRICE} \
  --region us-east-1 \
  --bootstrap-actions Name="graphmap",Path="s3://zhong-active/graphmapping/emr-boostrap-graphmapping.sh"
fi

